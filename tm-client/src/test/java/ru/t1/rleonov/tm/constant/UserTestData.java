package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.model.User;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User NEW_USER = new User();

    @NotNull
    public final static User ADMIN = new User();

    static {
        USER1.setLogin("user1");
        USER1.setPassword("user1");

        USER2.setLogin("user2");
        USER2.setPassword("user2");

        NEW_USER.setLogin("NEW_USER");
        NEW_USER.setPassword("NEW_USER");
        NEW_USER.setEmail("NEW_USER@EMAIL.RU");

        ADMIN.setLogin("admin");
        ADMIN.setPassword("admin");
        ADMIN.setRole(Role.ADMIN);
    }

}
