package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Project;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectTestData {

    @NotNull
    public final static Project PROJECT1 = new Project("Project1", "Project1");

    @NotNull
    public final static Project PROJECT2 = new Project("Project2", "Project2");

    @NotNull
    public final static Project PROJECT3 = new Project("Project3", "Project3");

    @NotNull
    public final static Project NEW_PROJECT = new Project("New Project", "New Project");

    @NotNull
    public final static List<Project> USER1_PROJECTS = Arrays.asList(PROJECT1, PROJECT2);

    @NotNull
    public final static List<Project> USER2_PROJECTS = Collections.singletonList(PROJECT3);

}
