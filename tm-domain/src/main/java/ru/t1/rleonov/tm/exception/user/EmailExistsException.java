package ru.t1.rleonov.tm.exception.user;

public final class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error! This email already exists...");
    }

}
