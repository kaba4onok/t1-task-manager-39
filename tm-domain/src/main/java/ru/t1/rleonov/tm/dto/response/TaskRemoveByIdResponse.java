package ru.t1.rleonov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable Task task) {
        super(task);
    }

}
