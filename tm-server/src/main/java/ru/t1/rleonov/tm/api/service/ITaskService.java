package ru.t1.rleonov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Task;
import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    String getSortType(@Nullable Sort sort);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task removeById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    @SneakyThrows
    List<Task> findAll();

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void set(@NotNull Collection<Task> tasks);

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

}
