package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.service.IProjectTaskService;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.comparator.NameComparator;
import ru.t1.rleonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.rleonov.tm.exception.field.TaskIdEmptyException;
import ru.t1.rleonov.tm.exception.field.UserIdEmptyException;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Task;
//import ru.t1.rleonov.tm.repository.ProjectRepository;
//import ru.t1.rleonov.tm.repository.TaskRepository;
import java.util.Collections;
import static ru.t1.rleonov.tm.constant.ProjectTestData.*;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    /*@NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void setUp() {
        projectRepository.set(ALL_PROJECTS);
        taskRepository.set(ALL_TASKS);
    }

    @After
    public void reset() {
        projectRepository.clear();
        taskRepository.clear();
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String userId = USER2_TASK1.getUserId();
        @NotNull final String projectId = USER2_PROJECT2.getId();
        @NotNull final String taskId = USER2_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.bindTaskToProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.bindTaskToProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.bindTaskToProject(userId, projectId, "0"));
        service.bindTaskToProject(userId, projectId, taskId);
        Assert.assertEquals(USER2_TASK1, taskRepository.findAllByProjectId(userId, projectId).get(0));
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        @NotNull final String taskId = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject(null, projectId, taskId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.unbindTaskFromProject("", projectId, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, null, taskId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(userId, "", taskId));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, projectId, null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(userId, projectId, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.unbindTaskFromProject(userId, "0", taskId));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.unbindTaskFromProject(userId, projectId, "0"));
        service.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAllByProjectId(userId, projectId));
    }

    @Test
    public void removeProjectById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_PROJECT1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeProjectById("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.removeProjectById(userId, ""));
        service.removeProjectById(userId, projectId);
        Assert.assertEquals(Collections.EMPTY_LIST, taskRepository.findAllByProjectId(userId, projectId));
    }*/

}
