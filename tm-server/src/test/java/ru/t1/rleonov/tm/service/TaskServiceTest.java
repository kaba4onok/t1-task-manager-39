package ru.t1.rleonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.service.ITaskService;
import ru.t1.rleonov.tm.comparator.CreatedComparator;
import ru.t1.rleonov.tm.enumerated.Sort;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.entity.TaskNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.marker.UnitCategory;
import ru.t1.rleonov.tm.model.Task;
//import ru.t1.rleonov.tm.repository.TaskRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.rleonov.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    /*@NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @Before
    public void setUp() {
        repository.set(ALL_TASKS);
    }

    @After
    public void reset() {
        repository.clear();
    }

    @Test
    public void create() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String name = USER1_TASK1.getName();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", name));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertEquals(name, repository.create(userId, name).getName());
    }

    @Test
    public void createWithDesc() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String name = USER1_TASK1.getName();
        @NotNull final String desc = USER1_TASK1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create("", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, "", desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, name, ""));
        Assert.assertEquals(name, repository.create(userId, name, desc).getName());
    }

    @Test
    public void updateById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        @NotNull final String name = USER1_TASK1.getName();
        @NotNull final String desc = USER1_TASK1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, id, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById("", id, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(userId, null, name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(userId, "", name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, id, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, id, "", desc));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.updateById(WRONG_USERID_TASK.getUserId(), id, name, desc));
        Assert.assertEquals(USER1_TASK2.getName(),
                service.updateById(userId, id, USER1_TASK2.getName(), desc).getName());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        @NotNull final Integer badIndex = USER1_TASKS.size() + 1;
        @NotNull final String name = USER1_TASK1.getName();
        @NotNull final String desc = USER1_TASK1.getDescription();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, index, name, desc));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex("", index, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, null, name, desc));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.updateByIndex(userId, badIndex, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, index, null, desc));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, index, "", desc));
        Assert.assertEquals(USER1_TASK2.getName(),
                service.updateByIndex(userId, index, USER1_TASK2.getName(), desc).getName());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById(null, id, status));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusById("", id, status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(userId, null, status));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(userId, "", status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusById(userId, id, null));
        Assert.assertThrows(TaskNotFoundException.class, () ->
                service.changeTaskStatusById(WRONG_USERID_TASK.getUserId(), id, status));
        Assert.assertEquals(status, service.changeTaskStatusById(userId, id, status).getStatus());
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        @NotNull final Integer badIndex = USER1_TASKS.size() + 1;
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex(null, index, status));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeTaskStatusByIndex("", index, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(userId, null, status));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeTaskStatusByIndex(userId, badIndex, status));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusByIndex(userId, index, null));
        Assert.assertEquals(status, service.changeTaskStatusByIndex(userId, index, status).getStatus());
    }

    @Test
    public void getSize() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(""));
        Assert.assertEquals(USER1_TASKS.size(), service.getSize(userId));
    }

    @Test
    public void add() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, USER1_TASK1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add("", USER1_TASK1));
        Assert.assertNull(service.add(userId, null));
        Assert.assertEquals(USER1_TASK1, service.add(USER1_TASK1));
    }

    @Test
    public void findAll() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        Assert.assertEquals(USER1_TASKS, service.findAll(userId));
    }

    @Test
    public void findAllComparator() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(null, Sort.BY_CREATED));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll("", Sort.BY_CREATED));
        @NotNull final List<Task> sortedTasks = new ArrayList<>(USER1_TASKS);
        sortedTasks.sort(CreatedComparator.INSTANCE);
        Assert.assertEquals(sortedTasks, service.findAll(userId, Sort.BY_CREATED));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, ""));
        Assert.assertTrue(service.existsById(userId, id));
    }

    @Test
    public void findOneById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, ""));
        Assert.assertEquals(USER1_TASK1, service.findOneById(userId, id));
    }

    @Test
    public void findOneByIndex() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        @NotNull final Integer badIndex = USER1_TASKS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, -1));
        Assert.assertEquals(USER1_TASK1, service.findOneByIndex(userId, index));
    }
    @Test
    public void remove() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(null, USER1_TASK1));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove("", USER1_TASK1));
        Assert.assertNull(service.remove(userId, null));
        Assert.assertEquals(USER1_TASK1, service.remove(USER1_TASK1));
    }

    @Test
    public void removeById() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String id = USER1_TASK1.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(null, id));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById("", id));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userId, ""));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById("0", id));
        Assert.assertEquals(USER1_TASK1, service.removeById(userId, id));
    }

    @Test
    public void removeByIndex() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final Integer index = USER1_TASKS.indexOf(USER1_TASK1);
        @NotNull final Integer badIndex = USER1_TASKS.size() + 1;
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex(null, index));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeByIndex("", index));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, badIndex));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.removeByIndex(userId, -1));
        Assert.assertEquals(USER1_TASK1, service.removeByIndex(userId, index));
    }

    @Test
    public void clear() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll((String) null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(""));
        service.clear(userId);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(userId));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final String userId = USER1_TASK1.getUserId();
        @NotNull final String projectId = USER1_TASK1.getProjectId();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAllByProjectId(null, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAllByProjectId("", projectId));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.findAllByProjectId(userId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.findAllByProjectId(userId, ""));
    }*/

}
